module.exports = (sequelize, Sequelize) => {
    const Case = require('./case.model')(sequelize, Sequelize)
    const User = require('./user.model')(sequelize, Sequelize)

    const CaseUser = sequelize.define("CaseUser", {
        caseId: {
            type: Sequelize.INTEGER,
            references: {
                model: Case,
                key: 'id'
            }
        },
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: User,
                key: 'id'
            }
        },
    }, {timestamps: false})

    return CaseUser
}