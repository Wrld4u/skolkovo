module.exports = (sequelize, Sequelize) => {
    const Label = sequelize.define("labels", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        projectId: {
            type: Sequelize.INTEGER,
        },
        label: {
            type: Sequelize.STRING
        },
        color: {
            type: Sequelize.STRING
        },
    }, {timestamps: false})

    return Label
}