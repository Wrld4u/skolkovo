module.exports = (sequelize, Sequelize) => {
    const Case = require('./case.model')(sequelize, Sequelize)
    const Label = require('./label.model')(sequelize, Sequelize)

    const CaseLabel = sequelize.define("CaseLabel", {
        caseId: {
            type: Sequelize.INTEGER,
            references: {
                model: Case,
                key: 'id'
            }
        },
        labelId: {
            type: Sequelize.INTEGER,
            references: {
                model: Label,
                key: 'id'
            }
        },
    }, {timestamps: false})

    return CaseLabel
}