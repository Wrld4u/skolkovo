module.exports = (sequelize, Sequelize) => {
    const Case = require('./case.model')(sequelize, Sequelize)
    const Cycle = require('./cycle.model')(sequelize, Sequelize)

    const CaseCycle = sequelize.define("CaseCycle", {
        caseId: {
            type: Sequelize.INTEGER,
            references: {
                model: Case,
                key: 'id'
            }
        },
        cycleId: {
            type: Sequelize.INTEGER,
            references: {
                model: Cycle,
                key: 'id'
            }
        },
    }, {timestamps: false})

    return CaseCycle
}