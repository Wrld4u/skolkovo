module.exports = (sequelize, Sequelize) => {
    const Project = require('./project.model')(sequelize, Sequelize)
    const User = require('./user.model')(sequelize, Sequelize)

    const ProjectUser = sequelize.define("ProjectUser", {
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: User,
                key: 'id'
            }
        },
        projectId: {
            type: Sequelize.INTEGER,
            references: {
                model: Project,
                key: 'id'
            }
        },
    }, {timestamps: false})

    return ProjectUser
}