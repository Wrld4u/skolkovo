module.exports = (sequelize, Sequelize) => {
    const Case = sequelize.define("cases", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        projectId: {
            type: Sequelize.INTEGER,
        },
        userId: {
            type: Sequelize.INTEGER,
        },
        cycleId: {
            type: Sequelize.INTEGER,
        },
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.STRING
        },
        priority: {
            type: Sequelize.STRING
        },
        component: {
            type: Sequelize.STRING,
        },
        estimate: {
            type: Sequelize.STRING,
        },
        executorType: {
            type: Sequelize.STRING,
        },
        uploaded: {
            type: Sequelize.DATE,
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate: Sequelize.NOW,
        },
    })

    return Case
}