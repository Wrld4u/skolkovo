module.exports = (sequelize, Sequelize) => {
    const Release = require('./release.model')(sequelize, Sequelize)
    const Cycle = require('./cycle.model')(sequelize, Sequelize)

    const CycleRelease = sequelize.define("CycleRelease", {
        releaseId: {
            type: Sequelize.INTEGER,
            references: {
                model: Release,
                key: 'id'
            }
        },
        cycleId: {
            type: Sequelize.INTEGER,
            references: {
                model: Cycle,
                key: 'id'
            }
        },
    }, {timestamps: false})

    return CycleRelease
}