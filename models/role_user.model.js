module.exports = (sequelize, Sequelize) => {
    const USER = require('./user.model')(sequelize, Sequelize)
    const ROLE = require('./role.model')(sequelize, Sequelize)

    const RoleUser = sequelize.define("RoleUser", {
        roleId: {
            type: Sequelize.INTEGER,
            references: {
                model: ROLE,
                key: 'id'
            }
        },
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: USER,
                key: 'id'
            }
        },
    }, {timestamps: false})

    return RoleUser
}