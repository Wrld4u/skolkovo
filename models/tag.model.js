module.exports = (sequelize, Sequelize) => {
    const Tag = sequelize.define("tags", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tag: {
            type: Sequelize.STRING
        },
        color: {
            type: Sequelize.STRING
        },
    }, {timestamps: false})

    return Tag
}