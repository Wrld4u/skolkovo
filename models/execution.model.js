module.exports = (sequelize, Sequelize) => {
    const Exec = sequelize.define("executions", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        caseId: {
            type: Sequelize.INTEGER,
        },
        releaseId: {
            type: Sequelize.INTEGER,
        },
        userId: {
            type: Sequelize.INTEGER,
        },
        cycleId: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.STRING
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate : Sequelize.NOW,
        },
    })

    return Exec
}