module.exports = (sequelize, Sequelize) => {
    const Step = sequelize.define("step", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        caseId: {
            type: Sequelize.INTEGER,
        },
        step: {
            type: Sequelize.STRING
        },
        data: {
            type: Sequelize.STRING
        },
        result: {
            type: Sequelize.STRING
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate : Sequelize.NOW,
        },
    })

    return Step
}