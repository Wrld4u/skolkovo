module.exports = (sequelize, Sequelize) => {
    const Project = require('./project.model')(sequelize, Sequelize)
    const Tag = require('./tag.model')(sequelize, Sequelize)

    const ProjectTag = sequelize.define("ProjectTag", {
        tagId: {
            type: Sequelize.INTEGER,
            references: {
                model: Tag,
                key: 'id'
            }
        },
        projectId: {
            type: Sequelize.INTEGER,
            references: {
                model: Project,
                key: 'id'
            }
        },
    }, {timestamps: false})

    return ProjectTag
}