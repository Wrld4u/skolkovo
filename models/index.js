const config = require('config')
const Sequelize = require('sequelize')

const dbConfig = config.get('DBConn')

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    // operatorsAliases: false,
    logging: dbConfig.logging,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    },
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

// Company
db.company = require('./company.model')(sequelize, Sequelize)

// USERS
db.user = require('./user.model')(sequelize, Sequelize)
db.role = require('./role.model')(sequelize, Sequelize)
db.role_user = require('./role_user.model')(sequelize, Sequelize)
// role users reference, on delete user -> deleting role_user
db.user.belongsToMany(db.role, { through: db.role_user, onDelete: 'CASCADE' })
db.role.belongsToMany(db.user, { through: db.role_user, onDelete: 'CASCADE' })
//
db.user.belongsTo(db.company, {foreignKey: 'companyId'})
db.company.hasMany(db.user, {foreignKey: 'companyId'})

// PROJECTS
db.project = require('./project.model')(sequelize, Sequelize)
db.tag = require('./tag.model')(sequelize, Sequelize)
db.project_tag = require('./project_tag.model')(sequelize, Sequelize)
db.project_user = require('./project_user.model')(sequelize, Sequelize)

db.project.belongsToMany(db.tag, { through: db.project_tag, onDelete: 'CASCADE' })
db.tag.belongsToMany(db.project, { through: db.project_tag, onDelete: 'CASCADE' })

db.project.belongsToMany(db.user, { through: db.project_user, onDelete: 'CASCADE' })
db.user.belongsToMany(db.project, { through: db.project_user, onDelete: 'CASCADE' })

db.project.belongsTo(db.company, {foreignKey: 'companyId'})
db.company.hasMany(db.project, {foreignKey: 'companyId'})

// LABELS
db.label = require('./label.model')(sequelize, Sequelize)

// RECORDS
db.record = require('./record.model')(sequelize, Sequelize)

// AGENT
db.agent = require('./agent.model')(sequelize, Sequelize)
// Container
db.container = require('./container.model')(sequelize, Sequelize)
// Variable
db.variable = require('./variable.model')(sequelize, Sequelize)

// CYCLE
db.cycle = require('./cycle.model')(sequelize, Sequelize)

db.project.hasMany(db.cycle, {foreignKey: 'projectId'})
db.cycle.belongsTo(db.project, {foreignKey: 'projectId', onDelete: 'CASCADE'})

db.user.hasMany(db.cycle, {foreignKey: 'userId'})
db.cycle.belongsTo(db.user, {foreignKey: 'userId', onDelete: 'CASCADE'})

// RELEASES
db.release = require('./release.model')(sequelize, Sequelize)

db.project.hasMany(db.release, {foreignKey: 'projectId'})
db.release.belongsTo(db.project, {foreignKey: 'projectId', onDelete: 'CASCADE'})

db.user.hasMany(db.release, {foreignKey: 'userId'})
db.release.belongsTo(db.user, {foreignKey: 'userId', onDelete: 'CASCADE'})

db.cycle_release = require('./cycle_release.model')(sequelize, Sequelize)

db.release.belongsToMany(db.cycle, { through: db.cycle_release, onDelete: 'CASCADE' })
db.cycle.belongsToMany(db.release, { through: db.cycle_release, onDelete: 'CASCADE' })

// CASE
db.case = require('./case.model')(sequelize, Sequelize)
db.case_cycle = require('./case_cycle.model')(sequelize, Sequelize)
db.case_label = require('./case_label.model')(sequelize, Sequelize)
db.case_user = require('./case_user.model')(sequelize, Sequelize)

db.user.hasMany(db.case, {foreignKey: 'userId'})
db.case.belongsTo(db.user, {foreignKey: 'userId', onDelete: 'CASCADE'})

db.project.hasMany(db.case, {foreignKey: 'projectId'})
db.case.belongsTo(db.project, {foreignKey: 'projectId', onDelete: 'CASCADE'})

db.case.belongsToMany(db.cycle, { through: db.case_cycle, onDelete: 'CASCADE' })
db.cycle.belongsToMany(db.case, { through: db.case_cycle, onDelete: 'CASCADE' })

db.case.belongsToMany(db.label, { through: db.case_label, onDelete: 'CASCADE' })
db.label.belongsToMany(db.case, { through: db.case_label, onDelete: 'CASCADE' })

db.case.belongsToMany(db.user, { through: db.case_user, onDelete: 'CASCADE' })
db.user.belongsToMany(db.case, { through: db.case_user, onDelete: 'CASCADE' })

// EXECUTION
db.execution = require('./execution.model')(sequelize, Sequelize)

// db.case.hasMany(db.execution, {foreignKey: 'caseId'})
db.release.hasMany(db.execution, {foreignKey: 'releaseId'})
db.user.hasMany(db.execution, {foreignKey: 'userId'})
db.cycle.hasMany(db.execution, {foreignKey: 'cycleId'})

// db.execution.belongsTo(db.case, {foreignKey: 'caseId', onDelete: 'CASCADE'})
db.execution.belongsTo(db.release, {foreignKey: 'releaseId', onDelete: 'CASCADE'})
db.execution.belongsTo(db.user, {foreignKey: 'userId', onDelete: 'CASCADE'})
db.execution.belongsTo(db.cycle, {foreignKey: 'cycleId', onDelete: 'CASCADE'})

// STEP
db.step = require('./step.model')(sequelize, Sequelize)

db.case.hasMany(db.step, {foreignKey: 'caseId'})
db.step.belongsTo(db.case, {foreignKey: 'caseId', onDelete: 'CASCADE'})


module.exports = db