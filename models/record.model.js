module.exports = (sequelize, Sequelize) => {
    const Record = sequelize.define("records", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        projectId: {
            type: Sequelize.INTEGER,
        },
        caseId: {
            type: Sequelize.INTEGER,
        },
        userId: {
            type: Sequelize.INTEGER,
        },
        browser: {
            type: Sequelize.STRING
        },
        os: {
            type: Sequelize.STRING
        },
        server: {
            type: Sequelize.STRING
        },
        data: {
            type: Sequelize.JSON
        },
        description: {
            type: Sequelize.STRING
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate : Sequelize.NOW,
        },
    })

    return Record
}