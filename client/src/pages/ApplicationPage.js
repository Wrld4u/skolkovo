import React from 'react'

export const ApplicationPage = () => {







    const downloadPlayer = () => {
        let link = document.createElement('a')
        link.href = '/assets/presets/player-1.0-win.zip'
        link.setAttribute('download', 'player-1.0-win.zip')
        // message('Скачивается')
        document.body.appendChild(link)
        link.click()
        link.parentNode.removeChild(link)
      }


      const downloadRecorder = () => {
        let link = document.createElement('a')
        link.href = '/assets/presets/recorder-1.0-win.zip'
        link.setAttribute('download', 'recorder-1.0-win.zip')
        // message('Скачивается')
        document.body.appendChild(link)
        link.click()
        link.parentNode.removeChild(link)
      }









    return (
        <>
            <div className="row clear-row mt-noHeader">
                <div className="col-auto">
                    <h5>Приложения</h5>
                    <p className="txt-gray">Управляйте своими авторизованными приложениями</p>
                </div>
            </div>

            <div className="row clear-row flex-row">
                <div className="col mt-1 s10 ml-0 pl-0">
                    <p style={{ fontSize: '1.1rem' }}>Приложение записи</p>
                    <p className="txt-gray">Это приложение предназначено для записи взаимодействия пользователей.</p>

                    <button
                        className="waves-effect waves-light btn blue lighten-1 noUpper mt-1"
                        onClick={downloadRecorder}
                    >
                        Скачать
                    </button>
                </div>
            </div>

            <div className="row clear-row flex-row">
                <div className="col s10 mt-1 ml-0 pl-0">
                    <p style={{ fontSize: '1.1rem' }}>Приложение воспроизведения</p>
                    <p className="txt-gray">Это приложение предназначено для воспроизведения уже записанных пользовательских взаимодействий.</p>

                    <button
                        className="waves-effect waves-light btn blue lighten-1 noUpper mt-1"
                        onClick={downloadPlayer}
                    >
                        Скачать
                    </button>
                </div>
            </div>

        </>
    )
}