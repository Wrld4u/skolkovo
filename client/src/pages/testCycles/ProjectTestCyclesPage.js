import React, {useCallback, useContext, useEffect, useState} from 'react'
import {useHistory, useParams} from "react-router-dom"
import {SidebarContext} from "../../context/SidebarContext"
import {Loader} from "../partials/Loader"
import {AuthContext} from "../../context/AuthContext"
import {useHttp} from "../../hooks/http.hook"
import {useMessage} from "../../hooks/message.hook"
import {Header} from "../partials/Header"
import {NoTable} from "../partials/NoTable"

// NOT USED (Using TestCyclesPage) see routes
export const ProjectTestCyclesPage = () => {
    const { token, logout } = useContext(AuthContext)
    const {loading, request, error, clearError} = useHttp()
    const id = useParams().id || null
    const prj = useContext(SidebarContext)
    const [users, setUsers] = useState(null)
    const [project, setProject] = useState(null)
    const [projects, setProjects] = useState(null)
    const message = useMessage()
    const history = useHistory()
    const [cycles, setCycles] = useState([])
    const [cases, setCases] = useState([])

    useEffect(() => {
        message(error)
        if (error === 'Нет авторизации') {
            clearError()
            logout()
            history.push('/')
        }
        clearError()
    }, [error, message, clearError])

    // Активация input для materialize
    useEffect(() => {
        try {
            window.M.updateTextFields()
            window.M.FormSelect.init(document.querySelectorAll('select'),  {classes: 'black-text'})
            let elems = document.querySelectorAll('.modal');
            // let instances = window.M.Modal.init(elems, {});
            window.M.Modal.init(elems, {});
        } catch (e) {
            console.log(e)
        }
    })

    const getProject = useCallback(async (token, id) => {
        try {
            if (token && id) {
                const data = await request(`/api/project/${id}`, 'GET', null, {authorization: 'Bearer ' + token})
                setProject(data.project)
            }
            if (token) {
                const data = await request(`/api/project/projects`, 'POST', null, {authorization: 'Bearer ' + token})
                setProjects(data.projects)
            }
        } catch (e) {
            console.log(e)
        }
    }, [request])

    const getCycles = useCallback(async (token, id) => {
        try {
            if (token && id) {
                const data = await request(`/api/cycle/all`, 'POST', {projectId: id}, {authorization: 'Bearer ' + token})
                data.cycles = data.cycles.map(el => {
                    el.showDetails = false
                    return el
                })
                setCycles(data.cycles)
                // console.log(data.cycles)
            }
            if (token && !id) {
                const data = await request(`/api/cycle/all`, 'POST', {projectId: null}, {authorization: 'Bearer ' + token})
                data.cycles = data.cycles.map(el => {
                    el.showDetails = false
                    return el
                })
                setCycles(data.cycles)
            }
        } catch (e) {
            console.log(e)
        }
    }, [request])

    // get users data
    const fetchUsers = useCallback(async () => {
        try {
            const data = await request(`/api/auth/users`, 'POST', null, {authorization: 'Bearer ' + token})
            if (data.users) {
                setUsers(data.users)
            }
        } catch (e) {
            console.log(e)
        }
    }, [request, setUsers])

    const getCases = useCallback(async (token, id) => {
        try {
            if (token && id) {
                const data = await request(`/api/case/all`, 'POST', {projectId: id}, {authorization: 'Bearer ' + token})
                setCases(data.cases)
            }
        } catch (e) {
            console.log(e)
        }
    }, [request])

    useEffect(() => {
        try {
            prj.toggle(false, id)
            fetchUsers()
            getProject(token, id)
            getCycles(token, id)
            getCases(token, id)
        } catch (e) {
            console.log(e)
        }
    }, [fetchUsers, id, getCycles, getCases, getProject])

    if (loading) {
        return <Loader />
    }

    const createCycle = () => {
        if (id) {
            history.push(`/project/${id}/createCycle`)
        } else {
            history.push(`/testCycles/createCycle`)
        }
    }

    const createTestCase = () => {
        if (id) {
            history.push(`/project/${id}/createTestCase`)
        } else {
            history.push(`/testCases/createTestCase`)
        }
    }

    const deleteHandler = async (cycleId) => {
        try {
            const data = await request(`/api/cycle/${cycleId}`, 'DELETE', null, {authorization: 'Bearer ' + token})
            message(data.message)

            setCycles([...cycles.filter(el => el.id !== cycleId)])
        } catch (e) {}
    }

    const unlinkCaseHandler = async (caseId, cycleId) => {
        console.log(`Unlink: Case ${caseId}, cycle ${cycleId}`)
        await request(`/api/cycle/unlink`, 'POST', {caseId, cycleId}, {authorization: 'Bearer ' + token})
        let idx = cycles.findIndex(el => el.id === cycleId)
        let t = [...cycles]
        t[idx].cases = t[idx].cases.filter(el => el.id !== caseId)
        setCycles([...t])
    }

    const linkCaseHandler = async (caseId, cycleId) => {
        console.log(`Link: Case ${caseId}, cycle ${cycleId}`)
        const data = await request(`/api/cycle/link`, 'POST', {caseId, cycleId}, {authorization: 'Bearer ' + token})
        let idx = cycles.findIndex(el => el.id === cycleId)
        let t = [...cycles]
        t[idx].cases.push(data.c)
        setCycles([...t])
    }

    return (
        <>
            <Header params={{
                title: `Циклы [${id && project ? project.name : ''}]`,
                btnL: {
                    actionHandler: createCycle,
                    title: '+ Цикл',
                    display: 'inline-block'
                },
                btnR: {
                    actionHandler: createTestCase,
                    title: '+ Тест кейс',
                    display: 'inline-block'
                },
                loading
            }}/>

            <div className="row clear-row afterHeader">
                <div className="col-auto">
                    <h5>Тест кейсы</h5>
                    {/*<p className="txt-gray">Manage your project settings</p>*/}
                </div>
            </div>

            <div className="row clear-row flex-row">
                {cycles && cycles.length ? (
                    <div className="col s12 pl-0">
                        <table className="highlight">
                            <thead>
                            <tr>
                                <th className="txt-gray ">id</th>
                                <th className="txt-gray ">Имя</th>
                                <th className="txt-gray ">Статус</th>
                                <th className="txt-gray ">Описание</th>
                                <th className="txt-gray ">Тест кейсы</th>
                                {/*<th className="txt-gray center">Executions</th>*/}
                                <th className="txt-gray ">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            {cycles && cycles.length ? cycles.map(c => {
                                return(
                                    <React.Fragment key={c.id}>
                                        <tr
                                            style={{cursor: 'pointer'}}
                                            // onClick={() => {
                                            //     let idx = cycles.findIndex(el => el.id === c.id)
                                            //     let t = cycles.map(el => {
                                            //         if (el.id !== c.id) el.showDetails = false
                                            //         return el
                                            //     })
                                            //     t[idx].showDetails = !t[idx].showDetails
                                            //     setCycles([...t])
                                            //     // console.log(c)
                                            // }}
                                        >
                                            <td className="">{c.id}</td>
                                            <td className="td-titleMAW-250">{c.name || '-'}</td>
                                            <td className="">{c.status || '-'}</td>
                                            <td className="">{c.description || '-'}</td>
                                            <td className="">{c.cases.length || '-'}</td>
                                            {/*<td className="center">{c.executions.length || '-'}</td>*/}
                                            <td className="td-actions">
                                                <i
                                                    style={{cursor: 'pointer'}}
                                                    className="fa fa-pencil-square-o"
                                                    aria-hidden="true"
                                                    onClick={() => {history.push(`/project/${c.projectId}/testCycles/${c.id}`)}}
                                                />
                                                <i
                                                    style={{cursor: 'pointer'}}
                                                    className="fa fa-trash-o pl-1"
                                                    aria-hidden="true"
                                                    onClick={() => {deleteHandler(c.id)}}
                                                />
                                            </td>
                                        </tr>
                                        { c.showDetails ? (
                                            <>
                                            {/*CASES*/}
                                            <tr>
                                                <td></td>
                                                <td colSpan="4" className="center" style={{borderBottom: '2px solid grey'}}>

                                                    <table className="highlight">
                                                        <thead>
                                                        <tr>
                                                            <th className="txt-gray ">id</th>
                                                            <th className="txt-gray ">Описание</th>
                                                            <th className="txt-gray ">Статус</th>
                                                            <th className="txt-gray ">Компонент</th>
                                                            <th className="txt-gray ">Метки</th>
                                                            <th className="txt-gray ">Создатель</th>
                                                            <th className="txt-gray ">Дата создания</th>
                                                            <th className="txt-gray ">Действия</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {c.cases.length ? c.cases.map(el => {
                                                            return(
                                                                <tr
                                                                    key={el.id}
                                                                >
                                                                    <td className="">{el.id}</td>
                                                                    <td className="">{el.title || '-'}</td>
                                                                    <td className="">{el.status}</td>
                                                                    <td className="">{el.component}</td>
                                                                    <td className="">{el.labels.length ? el.labels.map(l => l.label).join(', ') : '-'}</td>
                                                                    <td className="">{users && users.length ? users.find(el => el.id === c.userId).email : '-'}</td>
                                                                    <td className="">{el.updatedAt ? new Date(el.updatedAt).toLocaleDateString() : '-'}</td>
                                                                    <td className="">
                                                                        <i
                                                                            style={{cursor: 'pointer'}}
                                                                            className="fa fa-plus modal-trigger"
                                                                            aria-hidden="true"
                                                                            data-target="modal1"
                                                                        />
                                                                        <i
                                                                            style={{cursor: 'pointer'}}
                                                                            className="fa fa-trash-o pl-1"
                                                                            aria-hidden="true"
                                                                            onClick={() => {unlinkCaseHandler(el.id, c.id)}}
                                                                        />
                                                                    </td>
                                                                </tr>
                                                            )
                                                        }) : (
                                                            <tr
                                                                // onClick={() => {linkCaseHandler(el.id)}}
                                                                data-target="modal1"
                                                                className="modal-trigger"
                                                            >
                                                                <td colSpan="8" className="center">
                                                                    <ul><li style={{cursor: 'pointer'}}>+ Привязать тест кейс к этому тестовому циклу</li></ul>
                                                                </td>
                                                            </tr>
                                                        )}
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>

                                            {/*/!*EXECUTIONS*!/*/}
                                            {/*<tr>*/}
                                            {/*<td></td>*/}
                                            {/*<td></td>*/}
                                            {/*<td colSpan="4" className="center" style={{borderBottom: '2px solid green'}}>*/}
                                            {/*<table className="highlight">*/}
                                            {/*<thead>*/}
                                            {/*<tr>*/}
                                            {/*<th className="txt-gray center">id</th>*/}
                                            {/*<th className="txt-gray center">Test Cycle</th>*/}
                                            {/*<th className="txt-gray center">Project</th>*/}
                                            {/*<th className="txt-gray center">Status</th>*/}
                                            {/*<th className="txt-gray center">Executed By</th>*/}
                                            {/*<th className="txt-gray center">Executed On</th>*/}
                                            {/*</tr>*/}
                                            {/*</thead>*/}
                                            {/*<tbody>*/}
                                            {/*{c.executions && c.executions.length ? c.executions.map(exec => {*/}
                                            {/*return(*/}
                                            {/*<tr*/}
                                            {/*key={exec.id}*/}
                                            {/*// onClick={() => {history.push('/blog/' + post._id)}}*/}
                                            {/*>*/}
                                            {/*/!*<td className="center">{releases.length ? releases.find(el => el.id === exec.releaseId).name : '-'}</td>*!/*/}
                                            {/*/!*<td className="center">{cycles.length ? cycles.find(el => el.id === exec.cycleId).name : '-'}</td>*!/*/}
                                            {/*<td className="center">{exec.id}</td>*/}
                                            {/*<td className="center">{c.name}</td>*/}
                                            {/*<td className="center">{project.name}</td>*/}
                                            {/*<td className="center py-0">*/}
                                            {/*<div className={`input-field pl-1 bRadius-7 ${exec.status === 'Unexecuted' ? 'bgGray' : exec.status === 'Success' ? 'bgGreen' : 'bgRed'}`}>*/}
                                            {/*<select*/}
                                            {/*id={'status'+exec.id}*/}
                                            {/*name="status"*/}
                                            {/*value={exec.status}*/}
                                            {/*// onChange={(e) => {changeExecHandler(e, exec.id)}}*/}
                                            {/*disabled*/}
                                            {/*>*/}
                                            {/*<option value="Unexecuted">Unexecuted</option>*/}
                                            {/*<option value="Success">Success</option>*/}
                                            {/*<option value="Fail">Fail</option>*/}
                                            {/*</select>*/}
                                            {/*</div>*/}
                                            {/*</td>*/}
                                            {/*<td className="center">{users ? users.find(el => el.id === exec.userId).name : '-'}</td>*/}
                                            {/*<td className="center">{exec.updatedAt ? new Date(exec.updatedAt).toLocaleDateString() : '-'}</td>*/}
                                            {/*</tr>*/}
                                            {/*)*/}
                                            {/*}) : (<tr><td colSpan="7" className="center">There are no executions</td></tr>)}*/}
                                            {/*</tbody>*/}
                                            {/*</table>*/}
                                            {/*</td>*/}
                                            {/*</tr>*/}
                                            </>
                                        ) : (<></>)
                                        }
                                    </React.Fragment>
                                )
                            }) : (<></>)}
                            </tbody>
                            <tfoot>
                            {/*<tr onClick={addExecHandler}><td colSpan="7" className="center"><ul><li style={{cursor: 'pointer'}}>+ Add new execution</li></ul></td></tr>*/}
                            </tfoot>
                        </table>
                    </div>
                ) : (<NoTable params={{ title: `Нет тестовых циклов` }} />)}
            </div>

            <div id="modal1" className="modal">
                <div className="modal-content">
                    <h4>Тест кейсы</h4>
                    <table className="highlight">
                        <thead>
                        <tr>
                            <th className="txt-gray ">id</th>
                            <th className="txt-gray ">Описание</th>
                            <th className="txt-gray ">Статус</th>
                            <th className="txt-gray ">Компонент</th>
                            <th className="txt-gray ">Метки</th>
                            <th className="txt-gray ">Создатель</th>
                            <th className="txt-gray ">Дата создания</th>
                            <th className="txt-gray ">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        {cycles && cases && cases.length ? cases.map(c => {
                            let idx = cycles.findIndex(el => el.showDetails)

                            if (cycles[idx] && !cycles[idx].cases.filter(el => el.id === c.id).length) {
                                return(
                                    <tr
                                        key={c.id}
                                    >
                                        <td className="">{c.id}</td>
                                        <td className="">{c.title || '-'}</td>
                                        <td className="">{c.status}</td>
                                        <td className="">{c.component}</td>
                                        <td className="">{c.labels.length ? c.labels.map(l => l.label).join(', ') : '-'}</td>
                                        <td className="">{users ? users.find(el => el.id === c.userId).name : '-'}</td>
                                        <td className="">{c.updatedAt ? new Date(c.updatedAt).toLocaleDateString() : '-'}</td>
                                        <td className="">
                                            <i
                                                style={{cursor: 'pointer'}}
                                                className="fa fa-check-square-o modal-close"
                                                aria-hidden="true"
                                                onClick={() => {linkCaseHandler(c.id, cycles[idx].id)}}
                                            />
                                        </td>
                                    </tr>
                                )
                            } else { return (<React.Fragment key={c.id}></React.Fragment>) }
                        }) : (<tr><td colSpan="7" className="center">нет тест кейсов</td></tr>)}
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
                <div className="modal-footer">
                    <a href="#!" className="modal-close waves-effect waves-green btn-flat">Закрыть</a>
                </div>
            </div>
        </>
    )

}