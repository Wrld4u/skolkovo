import React, {useCallback, useContext, useEffect, useState} from 'react'
import {useHistory, useParams} from "react-router-dom"
import {SidebarContext} from "../../context/SidebarContext"
import {Loader} from "../partials/Loader"
import {AuthContext} from "../../context/AuthContext"
import {useHttp} from "../../hooks/http.hook"
import {useMessage} from "../../hooks/message.hook"
import {Header} from "../partials/Header"
import {NoTable} from "../partials/NoTable"

// Not Used Now
export const AgentsPage = () => {
    const { token, logout } = useContext(AuthContext)
    const {loading, request, error, clearError} = useHttp()
    const id = useParams().id
    const prj = useContext(SidebarContext)
    const [project, setProject] = useState(null)
    const [users, setUsers] = useState(null)
    const message = useMessage()
    const history = useHistory()
    const [agents, setAgents] = useState([])

    // logout + error
    useEffect(() => {
        message(error)
        if (error === 'Нет авторизации') {
            clearError()
            logout()
            history.push('/')
        }
        clearError()
    }, [error, message, clearError])

    // Активация input для materialize
    useEffect(() => {
        window.M.updateTextFields()
    })

    const getProject = useCallback(async (token, id) => {
        if (token && id) {
            const data = await request(`/api/project/${id}`, 'GET', null, {authorization: 'Bearer ' + token})
            setProject(data.project)
        }
    }, [])

    // get users data
    const fetchUsers = useCallback(async () => {
        try {
            const data = await request(`/api/auth/users`, 'POST', null, {authorization: 'Bearer ' + token})
            if (data.users) {
                setUsers(data.users)
            }
        } catch (e) {
            console.log(e)
        }
    }, [request, setUsers])

    const getAgents = useCallback(async (token, id) => {
        try {
            if (token && id) {
                const data = await request(`/api/agent/all`, 'POST', {projectId: id}, {authorization: 'Bearer ' + token})
                setAgents(data.agents)
            }
        } catch (e) {
            console.log(e)
        }
    }, [])

    useEffect(() => {
        try {
            prj.toggle(false, id)
            getProject(token, id)
            getAgents(token, id)
            fetchUsers()
        } catch (e) {
            console.log(e)
        }
    }, [id])

    if (!prj.id || !project || loading) {
        return <Loader />
    }

    // header buttons
    const createAgent = () => {
        history.push(`/project/${prj.id}/createAgent`)
    }

    // header buttons
    const createTestCase = () => {
        history.push(`/project/${prj.id}/createTestCase`)
    }

    const deleteHandler = async (id) => {
        try {
            const data = await request(`/api/agent/${id}`, 'DELETE', null, {authorization: 'Bearer ' + token})
            message(data.message)

            let agent = agents.filter(el => el.id !== id)
            setAgents([...agent])
        } catch (e) {}
    }

    return (
        <>
            <Header params={{
                title: `Агенты [${project.name}]`,
                btnL: {
                    actionHandler: createAgent,
                    title: '+ Агент',
                    display: 'inline-block'
                },
                btnR: {
                    actionHandler: createTestCase,
                    title: '+ Тест кейс',
                    display: 'inline-block'
                },
                loading
            }}/>

            <div className="row clear-row afterHeader">
                <div className="col-auto">
                    <h5>Агенты</h5>
                    {/*<p className="txt-gray">Manage your project settings</p>*/}
                </div>
            </div>

            <div className="row clear-row flex-row">
                {agents && agents.length ? (
                    <div className="col s12 pl-0">
                        <table className="highlight">
                            <thead>
                            <tr>
                                <th className="txt-gray ">id</th>
                                <th className="txt-gray ">Имя</th>
                                <th className="txt-gray ">Данные</th>
                                <th className="txt-gray ">IP</th>
                                <th className="txt-gray ">Описание</th>
                                <th className="txt-gray ">Пользователь</th>
                                <th className="txt-gray ">Проект</th>
                                <th className="txt-gray ">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            {agents && agents.length ? agents.map(c => {
                                return(
                                    <tr
                                        key={c.id}
                                    >
                                        <td className="center">{c.id}</td>
                                        <td className="center">{c.name || '-'}</td>
                                        <td className="center">{c.data || '-'}</td>
                                        <td className="center">{c.ip || '-'}</td>
                                        <td className="center">{c.description || '-'}</td>
                                        <td className="center">{users ? users.find(el => el.id === c.userId).name : '-'}</td>
                                        <td className="center">{project ? project.name : '-'}</td>
                                        <td className="center">
                                            <i
                                                style={{cursor: 'pointer'}}
                                                className="fa fa-pencil-square-o"
                                                aria-hidden="true"
                                                // onClick={() => {history.push(`/project/${prj.id}/testCases/${c.id}`)}}
                                            />
                                            <i
                                                style={{cursor: 'pointer'}}
                                                className="fa fa-trash-o pl-1"
                                                aria-hidden="true"
                                                onClick={() => {deleteHandler(c.id)}}
                                            />
                                        </td>
                                    </tr>
                                )
                            }) : (<></>)}
                            </tbody>
                            <tfoot>
                            {/*<tr onClick={addExecHandler}><td colSpan="7" className="center"><ul><li style={{cursor: 'pointer'}}>+ Add new execution</li></ul></td></tr>*/}
                            </tfoot>
                        </table>
                    </div>
                ) : (<NoTable params={{ title: `Агентов нет` }} />)}
            </div>
        </>
    )

}