import {createContext} from 'react'

function noop() {}

export const SidebarContext = createContext({
    isProject: false,
    id: null,
    toggle: noop
})