const express = require('express')
const config = require('config')
const path = require('path')
const fileUpload = require('express-fileupload')
const cors = require('cors')
const app = express()


app.use(cors())
app.use(express.json({ extended: true }))
app.use(fileUpload({ createParentPath: true }))

app.use('/api/auth', require('./routes/auth.routes'))
app.use('/api/project', require('./routes/project.routes'))
app.use('/api/label', require('./routes/label.routes'))
app.use('/api/cycle', require('./routes/cycle.routes'))
app.use('/api/release', require('./routes/release.routes'))
app.use('/api/exec', require('./routes/execution.routes'))
app.use('/api/case', require('./routes/case.routes'))
app.use('/api/agent', require('./routes/agent.routes'))
app.use('/api/container', require('./routes/container.routes'))
app.use('/api/variable', require('./routes/variable.routes'))
app.use('/api/record', require('./routes/record.routes'))
app.use('/api/step', require('./routes/step.routes'))
app.use('/api/search', require('./routes/search.routes'))

if (process.env.NODE_ENV === 'production') {
    app.use('/', express.static(path.join(__dirname, 'client', 'build')))

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
    })
}

const db = require("./models")
db.sequelize.sync()

// working test
app.get('/', async (req, res) => {
    res.status(200).json({message: `working good! :)`})
})

const PORT = process.env.PORT || config.get('port') || 5000

async function start() {
    try {
        app.listen(PORT, () => {
            console.log(`Server has been started on ${PORT}`)
        })
    } catch (e) {
        console.log(`Server error: ${e.message}`)
        process.exit(1)
    }
}

start()