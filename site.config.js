// Запуск этого приложения: pm2 start site.config.js

module.exports = {
    apps : [{
        name: "skolkovo tester",
        script: "./app.js",
        env: {
            NODE_ENV: "production",
            PORT: 5001
        },
        env_dev: {
            NODE_ENV: "production",
            PORT: 5001
        },
        // instances : "2",
        // exec_mode : "cluster"
    }]
}
