const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Agent = db.agent

const router = Router()


// route prefix = /api/agent
//
// create agent
router.post('/create', auth, async (req, res) => {
        try {
            const { projectId, userId, name, data, ip, description, executionId } = req.body

            const agent = await Agent.create({ projectId, userId, name, data, ip, description, executionId })

            return res.status(201).json({ message: 'Agent created' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
    })

// get agent by id
router.get('/:id', auth, async (req, res) => {
    try {
        const agent = await Agent.findByPk(req.params.id)

        if (!agent) {
            return res.status(400).json({ message: 'No such agent' })
        }

        res.json({ agent, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})


// get all agents
router.post('/all', auth, async (req, res) => {
    try {
        const { projectId } = req.body

        let agents = null

        if (projectId) {
            agents = await Agent.findAll({ where: {projectId} })
        } else {
            agents = await Agent.findAll({})
        }

        res.json({ agents, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// delete agent by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const agent = await Agent.findByPk(req.params.id)

        await agent.destroy()

        // todo delete all linked tables

        return res.status(202).json({ message: 'Агент удален' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// // get project by id
// router.get('/:id', auth, async (req, res) => {
//     try {
//         const project = await Project.findByPk(req.params.id)
//         // console.log('=============', await project.getTags())
//         project.tags = await project.getTags()
//         project.cycles = await project.getCycles()
//         project.releases = await project.getReleases()
//         res.json({ project, message: 'ok' })
//     } catch (e) {
//         res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
//     }
// })
//
// update project by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { form } = req.body

        console.log('form', form)
        console.log('id', req.params.id)

        const agent = await Agent.findByPk(req.params.id)

        if (!agent) {
            return res.status(400).json({ message: 'Нет такого агента' })
        }

        await agent.update({...form})

        return res.status(202).json({ message: 'Обновленоно' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

module.exports = router