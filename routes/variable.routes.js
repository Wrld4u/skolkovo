const { Router } = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Variable = db.variable

const router = Router()


// route prefix = /api/variable

// create var
router.post('/create', auth, async (req, res) => {
    console.log("____________________________________________")
    try {
        const { prjId, name, value, description } = req.body
        const variable = await Variable.create({ projectId: prjId, name, value, description })
        return res.status(201).json({ message: 'Переменная создана' })

    } catch (e) {
        console.log(e)
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

router.post('/update', auth, async (req, res) => {
    console.log("____________________________________________")
    try {
        const { id, prjId, name, value, description } = req.body
        const variable = await Variable.update({ projectId: prjId, name, value, description },
            {
                where: {
                    id
                },
            },
        )
        return res.status(201).json({ message: 'Переменная обновлена' })

    } catch (e) {
        console.log(e)
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})





router.post('/delete', auth, async (req, res) => {
    console.log("____________________________________________")
    try {
        const { id, projectId } = req.body
        const variableToDelete = await Variable.destroy(
            {
                where: {
                    id
                },
            },
        )

        const variables = await Variable.findAll({ where: { projectId } })
        return res.json({ variables, message: 'Переменная удалена' })
    } catch (e) {
        console.log(e)
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})


// get all var
router.post('/all', auth, async (req, res) => {
    try {
        const { projectId } = req.body

        const variables = await Variable.findAll({ where: { projectId } })


        res.json({ variables, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// get all  used in project
router.post('/usedInProject', auth, async (req, res) => {
    try {
        const { projectId } = req.body

        const variables = await Variable.findAll({ where: { projectId, used: true } })


        res.json({ variables, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})



module.exports = router