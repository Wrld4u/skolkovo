const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Project = db.project
const Tag = db.tag
const Cycle = db.cycle
const Release = db.release

const router = Router()


// route prefix = /api/project

// create project
router.post('/create', auth, async (req, res) => {
        try {
            const { name, description} = req.body
            const id = req.headers.id

            const project = await Project.create({ name, description})

            // set User
            await project.setUsers([id])

            return res.status(201).json({id: project.id, message: 'Проект создан' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
    })

// get project by id
router.get('/:id', auth, async (req, res) => {
    try {
        const project = await Project.findByPk(req.params.id)
        // console.log('=============', await project.getTags())
        // console.log('=============', await project.getCycles())
        // console.log('=============', await project.getReleases())
        // let tags = await project.getTags()
        let cycles = await project.getCycles()
        let releases = await project.getReleases()
        res.json({ project, tags: [], cycles, releases, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// get projects
router.post('/projects', auth, async (req, res) => {
    try {
        const projects = await Project.findAll({include:[Cycle, Release]})
        res.json({ projects, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// update project by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { name } = req.body

        const project = await Project.findByPk(req.params.id)

        project.name = name
        await project.save()

        return res.status(202).json({ project, message: 'Проект обновлён' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// delete project by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const project = await Project.findByPk(req.params.id)

        await project.destroy()

        // todo delete all linked tables

        return res.status(202).json({ message: 'Проект удалён' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

module.exports = router