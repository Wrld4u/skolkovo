const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Cycle = db.cycle
const Case = db.case
const Label = db.label
const Exec = db.execution
const Rel = db.release

const router = Router()


// route prefix = /api/cycle
//
// create cycle
router.post('/create', auth, async (req, res) => {
        try {
            const { form, execRows } = req.body

            const cycle = await Cycle.create({
                projectId: form.projectId,
                userId: form.userId,
                name: form.name,
                status: form.status,
                description: form.description
            })

            if (form.cases.length) {
                await cycle.setCases([...form.cases])
            }
            // // executors
            // if (execRows && execRows.length) {
            //     for (let i = 0; i < execRows.length; i++) {
            //         const exec = await Exec.create({
            //             caseId: 0,
            //             releaseId: 0,
            //             userId: form.userId,
            //             cycleId: cycle.id,
            //             status: execRows[i].status,
            //         })
            //     }
            // }


            return res.status(201).json({ message: 'Цикл создан' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
})

router.post('/link', auth, async (req, res) => {
        try {
            const { caseId, cycleId } = req.body

            const cycle = await Cycle.findByPk(cycleId)
            if (cycle) {
                // Link manyToMany Case - Cycle
                await cycle.addCases([caseId])
            }

            const c = await Case.findOne({where:{ id: caseId }, include: [{
                    model: Label,
                    attributes: ['id', 'label']
                }]})

            return res.status(201).json({ c, message: 'Linked' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
})

router.post('/unlink', auth, async (req, res) => {
        try {
            const { caseId, cycleId } = req.body

            const cycle = await Cycle.findByPk(cycleId)
            if (cycle) {
                // Link manyToMany Case - Cycle
                await cycle.removeCases([caseId])
            }

            return res.status(201).json({ message: 'Unlinked' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
})

// get all cycle
router.post('/all', auth, async (req, res) => {
    try {
        const { projectId } = req.body

        let cycles = null

        if (projectId) {
            cycles = await Cycle.findAll({ where: {projectId}, include: [{
                    model: Case,
                    include: [{
                        model: Label,
                        attributes: ['id', 'label']
                    }]
                },
                {
                    model: Exec
                },
                {
                    model: Rel
                },
                ] })
        } else {
            cycles = await Cycle.findAll({ include: [{
                    model: Case,
                    include: [{
                        model: Label,
                        attributes: ['id', 'label']
                    }]
                },
                {
                    model: Exec
                },
                {
                    model: Rel
                },
                ] })
        }

        res.json({ cycles, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// get cycle by id
router.get('/:id', auth, async (req, res) => {
    try {
        const cycle = await Cycle.findByPk(req.params.id)

        let cases = await cycle.getCases({attributes: ['id']})
        if (cases.length) {
            cases = cases.map(el => el.id)
        } else {
            cases = []
        }

        cycle.dataValues.cases = cases

        // let execRows = await cycle.getExecutions()

        // res.json({ cycle, execRows, message: 'ok' })
        res.json({ cycle, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// update cycle by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { form, execRows } = req.body

        const cycle = await Cycle.findByPk(req.params.id)

        await cycle.update({name: form.name, status: form.status, description: form.description, projectId: form.projectId})

        if (form.cases.length) {
            await cycle.setCases([])
            await cycle.setCases([...form.cases])
        }

        await cycle.save()

        // // executors
        // if (execRows && execRows.length) {
        //     for (let i = 0; i < execRows.length; i++) {
        //         if (execRows[i].id > 0) {
        //             const exec = await Exec.findByPk(execRows[i].id)
        //             await exec.update({
        //                 caseId: 0,
        //                 releaseId: 0,
        //                 userId: form.userId,
        //                 cycleId: cycle.id,
        //                 status: execRows[i].status,
        //             })
        //         } else {
        //             const exec = await Exec.create({
        //                 caseId: 0,
        //                 releaseId: 0,
        //                 userId: form.userId,
        //                 cycleId: cycle.id,
        //                 status: execRows[i].status,
        //             })
        //         }
        //     }
        // }

        return res.status(202).json({ cycle, message: 'Обновлено' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// delete cycle by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const cycle = await Cycle.findByPk(req.params.id)

        await cycle.destroy()

        // todo delete all linked tables

        return res.status(202).json({ message: 'Удалено' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

module.exports = router