const { Router } = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Case = db.case
const Cycle = db.cycle
const Prj = db.project
const Rel = db.release
const Step = db.step
const Exec = db.execution
const Label = db.label
const User = db.user
const Variable = db.variable
const multer = require('multer')
const router = Router()
const fs = require('fs')
const path = require('path')

Case.hasMany(Exec)
Exec.belongsTo(Case)

// route prefix = /api/case
//
// create case
router.post('/create', auth, async (req, res) => {
    try {
        const { caseForm, stepRows } = req.body

        const testCase = await Case.create({
            projectId: caseForm.projectId,
            userId: caseForm.userId,
            cycleId: caseForm.cycleId,
            title: caseForm.title,
            description: caseForm.description,
            status: caseForm.status,
            priority: caseForm.priority,
            component: caseForm.component,
            estimate: caseForm.estimate,
            executorType: caseForm.executorType,
        })

        // link manyToMany Case - Labels
        if (caseForm.labels.length) {
            await testCase.setLabels([...caseForm.labels])
        }

        //link users (Assignee) manyToMany to case
        if (caseForm.assignee.length) {
            await testCase.setUsers([...caseForm.assignee])
        }

        // Link manyToMany Case - Cycle
        // await testCase.setCycles([caseForm.cycleId])

        // Link Cycle - Release
        // const cycle = await Cycle.findByPk(caseForm.cycleId)
        // await cycle.setReleases([caseForm.release])

        // steps
        if (stepRows && stepRows.length) {
            for (let i = 0; i < stepRows.length; i++) {
                const step = await Step.create({
                    caseId: testCase.id,
                    step: stepRows[i].step,
                    data: stepRows[i].data,
                    result: stepRows[i].result,
                })
            }
        }

        // executors
        // if (execRows && execRows.length) {
        //     for (let i = 0; i < execRows.length; i++) {
        //         const exec = await Exec.create({
        //             caseId: testCase.id,
        //             releaseId: caseForm.release,
        //             userId: caseForm.userId,
        //             cycleId: caseForm.cycleId,
        //             status: execRows[i].status,
        //         })
        //     }
        // }

        return res.status(201).json({ message: 'Case created' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// update case by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { caseForm, stepRows, execRows } = req.body

        const testCase = await Case.findByPk(req.params.id)

        await testCase.update({
            projectId: caseForm.projectId,
            // userId: caseForm.userId,
            // cycleId: caseForm.cycleId,
            title: caseForm.title,
            description: caseForm.description,
            status: caseForm.status,
            priority: caseForm.priority,
            component: caseForm.component,
            estimate: caseForm.estimate,
            executorType: caseForm.executorType,
        })

        console.log(caseForm.usedVariables)

        let resetUsedVariables = await Variable.update({ used: false }, { where: {}, returning: true }) // reset used variables in false

        caseForm.usedVariables?.forEach(async variable => {
            await Variable.update({ used: true }, { where: { id: variable.id } })
        })

        // link manyToMany Case - Labels
        if (caseForm.labels.length) {
            await testCase.setLabels([])
            await testCase.setLabels([...caseForm.labels])
        }

        //link users (Assignee) manyToMany to case
        if (caseForm.assignee.length) {
            await testCase.setUsers([])
            await testCase.setUsers([...caseForm.assignee])
        }

        // Link manyToMany Case - Cycle
        // await testCase.setCycles([])
        // await testCase.setCycles([caseForm.cycleId])

        // Link Cycle - Release
        // const cycle = await Cycle.findByPk(caseForm.cycleId)
        // await cycle.setReleases([])
        // await cycle.setReleases([caseForm.release])

        // steps
        if (stepRows && stepRows.length) {
            for (let i = 0; i < stepRows.length; i++) {
                if (stepRows[i].id > 0) {
                    const step = await Step.findByPk(stepRows[i].id)
                    await step.update({
                        caseId: testCase.id,
                        step: stepRows[i].step,
                        data: stepRows[i].data,
                        result: stepRows[i].result,
                    })
                } else {
                    const step = await Step.create({
                        caseId: testCase.id,
                        step: stepRows[i].step,
                        data: stepRows[i].data,
                        result: stepRows[i].result,
                    })
                }
            }
        }

        // executors
        // if (execRows && execRows.length) {
        //     for (let i = 0; i < execRows.length; i++) {
        //         if (execRows[i].id > 0) {
        //             const exec = await Exec.findByPk(execRows[i].id)
        //             await exec.update({
        //                 caseId: testCase.id,
        //                 releaseId: caseForm.release,
        //                 userId: caseForm.userId,
        //                 cycleId: caseForm.cycleId,
        //                 status: execRows[i].status,
        //             })
        //         } else {
        //             const exec = await Exec.create({
        //                 caseId: testCase.id,
        //                 releaseId: caseForm.release,
        //                 userId: caseForm.userId,
        //                 cycleId: caseForm.cycleId,
        //                 status: execRows[i].status,
        //             })
        //         }
        //     }
        // }

        await testCase.save()

        return res.status(202).json({ testCase, message: 'Test Case Updated' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// get case by id
router.get('/:id', auth, async (req, res) => {
    try {
        const testCase = await Case.findByPk(req.params.id)

        const cycle = await Cycle.findByPk(testCase.cycleId)

        // try {
        //     let releases = await cycle.getReleases({attributes: ['id']})
        // } catch (e) {
        //     console.log(e)
        // }

        let lbl = await testCase.getLabels({ attributes: ['id'] })
        if (lbl.length) {
            lbl = lbl.map(el => el.id)
        } else {
            lbl = []
        }

        let assignee = await testCase.getUsers({ attributes: ['id'] })
        if (assignee.length) {
            assignee = assignee.map(el => el.id)
        } else {
            assignee = []
        }

        let caseForm = {
            id: testCase.id,
            projectId: testCase.projectId,
            userId: testCase.userId,
            cycleId: testCase.cycleId,
            title: testCase.title,
            description: testCase.description,
            status: testCase.status,
            priority: testCase.priority,
            component: testCase.component,
            estimate: testCase.estimate,
            executorType: testCase.executorType,
            labels: lbl,
            uploaded: testCase.uploaded,
            // release: releases[0].id,
            assignee
        }

        let stepRows = await testCase.getSteps()

        let execRows = await testCase.getExecutions()
        if (execRows && execRows.length) {
            for (let i = 0; i < execRows.length; i++) {
                let usrName = await User.findByPk(execRows[i].userId)
                let relName = await Rel.findByPk(execRows[i].releaseId)
                let cycName = await Cycle.findByPk(execRows[i].cycleId)
                let prjName = ''
                if (cycName) {
                    prjName = await Prj.findByPk(cycName.projectId)
                }

                execRows[i].dataValues.relName = relName ? relName.name : ''
                execRows[i].dataValues.cycName = cycName ? cycName.name : ''
                execRows[i].dataValues.prjName = prjName ? prjName.name : ''
                execRows[i].dataValues.usrName = usrName && usrName.name ? usrName.name : usrName.email
            }
        }

        res.json({ caseForm, stepRows, cycle, execRows, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// delete case by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const testCase = await Case.findByPk(req.params.id)

        // link manyToMany Case - Labels
        await testCase.setLabels([])

        // link manyToMany Case - Users
        await testCase.setUsers([])

        // Link manyToMany Case - Cycle
        await testCase.setCycles([])

        // Link Cycle - Release
        // const cycle = await Cycle.findByPk(testCase.cycleId)
        // await cycle.setReleases([])

        // steps
        await testCase.removeSteps()

        // executors
        // await testCase.removeExecutions()

        await testCase.destroy()

        return res.status(202).json({ message: 'Тест кейс удален' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// get all cases
router.post('/all', auth, async (req, res) => {
    try {
        const { projectId } = req.body

        let cases = null

        if (projectId) {
            cases = await Case.findAll({
                where: { projectId }, include: [{
                    model: Label,
                    attributes: ['id', 'label']
                },
                {
                    model: Cycle,
                },
                {
                    model: Exec,
                },
                ]
            })
        } else {
            cases = await Case.findAll({
                include: [{
                    model: Label,
                    attributes: ['id', 'label']
                },
                {
                    model: Cycle,
                },
                {
                    model: Exec,
                    // order: [[Exec, 'updatedAt', 'DESC']]
                },
                ]
            })
        }

        res.json({ cases, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// get all cases
router.post('/tasks', auth, async (req, res) => {
    try {
        const { projectId, userId } = req.body

        let cases = null

        if (projectId) {
            cases = await Case.findAll({
                where: { projectId, userId }, include: [{
                    model: Label,
                    attributes: ['id', 'label']
                }, {
                    model: Cycle,
                },
                {
                    model: Exec,
                },]
            })
        } else {
            cases = await Case.findAll({
                where: { userId }, include: [{
                    model: Label,
                    attributes: ['id', 'label']
                }, {
                    model: Cycle,
                },
                {
                    model: Exec,
                },]
            })
        }

        res.json({ cases, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})





router.post('/uploadTestScenario', auth, async (req, res) => {
    try {
        // let caseScenario = req.files.file
        // const userId = req.headers.id
        // console.log(caseScenario)
        // console.log(userId)

        if (!req.files) {
            return res.status(400).json({ message: 'Файл не загружен' })
        } else {
            //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
            let caseScenario = req.files.file
            const caseId = req.headers.id

            // Use the mv() method to place the file in upload directory (i.e. "uploads")
            if (process.env.NODE_ENV === 'production') {
                caseScenario.mv(`./client/build/assets/caseScenario/${caseId}.log`)
            } else {
                caseScenario.mv(`./client/public/assets/caseScenario/${caseId}.log`)
            }


            const caseRow = await Case.findByPk(caseId)
            if (caseRow) {
                caseRow.uploaded = new Date()
                await caseRow.save()
            }


            // const fileScenario = await User.findByPk(caseId)
            // if (!fileScenario) {
            //     return res.status(400).json({ message: 'Такого пользователя нет' })
            // }
            // // console.log(avatar, userId)
            // fileScenario.photo = `/ assets / caseScenario / ${ caseId }.log`
            // await fileScenario.save()

            //send response
            // res.send({
            //     status: true,
            //     message: 'File is uploaded',
            //     data: {
            //         name: avatar.name,
            //         mimetype: avatar.mimetype,
            //         size: avatar.size
            //     }
            // });

            return res.status(200).json({ message: 'Обновлено' })
            // res.json({ message: 'Обновлено' })
        }
    } catch (e) {
        res.status(500).json({ message: `Что - то пошло не так! Ошибка: ${e.message} ` })
    }
})



router.post('/downloadScenario/:caseId', auth, async (req, res) => {
    try {
        const caseId = req.params.caseId

        let absPath = ''
        if (process.env.NODE_ENV === 'production') {
            absPath = path.join(__dirname, '..', 'client', 'build', 'assets', 'caseScenario', `${caseId}.log`)
        } else {
            absPath = path.join(__dirname, '..', 'client', 'public', 'assets', 'caseScenario', `${caseId}.log`)
        }

        // Проверяем существование файла
        if (fs.existsSync(absPath)) {
            res.download(absPath, `${caseId}.log`, (err) => {
                if (err) {
                    res.status(500).json({ message: `Ошибка при загрузке файла: ${err.message} ` })
                }
            })
        } else {
            res.status(404).json({ message: 'Файл не найден' })
        }

    } catch (e) {
        res.status(500).json({ message: `Что - то пошло не так! Ошибка: ${e.message} ` })
    }
})




module.exports = router