const { Router } = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Container = db.container

const router = Router()


// route prefix = /api/container
//
// create container
router.post('/create', auth, async (req, res) => {
    try {
        const { projectId, userId, name, data, ip, description, executionId } = req.body

        const container = await Container.create({ projectId, userId, name, data, ip, description, executionId })

        return res.status(201).json({ message: 'Контейнер создан' })

    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// get container by id
router.get('/:id', auth, async (req, res) => {
    try {
        const container = await Container.findByPk(req.params.id)

        if (!container) {
            return res.status(400).json({ message: 'Нет такого контейнера' })
        }

        res.json({ container, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})


// get all container
router.post('/all', auth, async (req, res) => {
    try {
        const { projectId } = req.body

        let containers = null

        if (projectId) {
            containers = await Container.findAll({ where: { projectId } })
        } else {
            containers = await Container.findAll({})
        }

        res.json({ containers, message: 'ok' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// delete container by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const container = await Container.findByPk(req.params.id)

        await container.destroy()

        // todo delete all linked tables

        return res.status(202).json({ message: 'Контейнер удален' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

// // get project by id
// router.get('/:id', auth, async (req, res) => {
//     try {
//         const project = await Project.findByPk(req.params.id)
//         // console.log('=============', await project.getTags())
//         project.tags = await project.getTags()
//         project.cycles = await project.getCycles()
//         project.releases = await project.getReleases()
//         res.json({ project, message: 'ok' })
//     } catch (e) {
//         res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
//     }
// })
//
// update project by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { form } = req.body

        console.log('form', form)
        console.log('id', req.params.id)

        const container = await Container.findByPk(req.params.id)

        if (!container) {
            return res.status(400).json({ message: 'Нет такого контейнера' })
        }

        await container.update({ ...form })

        return res.status(202).json({ message: 'Обновлено' })
    } catch (e) {
        res.status(500).json({ message: `Что-то пошло не так! Ошибка: ${e.message}` })
    }
})

module.exports = router