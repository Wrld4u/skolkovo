const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const axios = require('axios')
const Record = db.record

const router = Router()


// route prefix = /api/record
//
// create record
router.post('/create', auth, async (req, res) => {
        try {
            const { projectId, userId, browser, os, server, data, description, caseId } = req.body

            const record = await Record.create({ projectId, userId, browser, os, server, data, description, caseId })

            return res.status(201).json({ message: 'Запись создана' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
    })

router.post('/save', async (req, res) => {
        try {
            const { metadata, startDate } = req.body

            const record = await Record.create({
                projectId: 0,
                userId: 0,
                browser: 'Chrome',
                os: metadata.os && metadata.os.toLowerCase().includes('mac') ? 'MacOs' : 'Windows',
                server: '',
                data: JSON.stringify(req.body),
                description: '',
                caseId: 0
            })

            return res.status(201).json({ message: 'Запись создана' })

        } catch (e) {
            console.log('ERROR ====================== \n', e.message)
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
    })

// play record by id
router.get('/play/:id', auth, async (req, res) => {
    try {
        const record = await Record.findByPk(req.params.id)

        if (!record) {
            return res.status(400).json({ message: 'Нет такой записи' })
        }

        // console.log('===========================')
        // console.log(JSON.parse(record.data))

        axios.post(`${record.server.trim()}/scenario`, JSON.parse(record.data), {headers: {'Content-Type': 'application/json'}})

        res.json({ message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// get all records
router.post('/all', auth, async (req, res) => {
    try {
        const { projectId } = req.body

        let records = null

        if (projectId) {
            records = await Record.findAll({ where: {projectId} })
        } else {
            records = await Record.findAll({})
        }

        res.json({ records, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// delete record by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const record = await Record.findByPk(req.params.id)

        await record.destroy()

        // todo delete all linked tables

        return res.status(202).json({ message: 'Запись удалена' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// get record by id
router.get('/:id', auth, async (req, res) => {
    try {
        const record = await Record.findByPk(req.params.id)

        if (!record) {
            return res.status(400).json({ message: 'Нет такой записи' })
        }

        res.json({ record, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// update record by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { form } = req.body

        const record = await Record.findByPk(req.params.id)

        if (!record) {
            return res.status(400).json({ message: 'Нет такой записи' })
        }

        await record.update({...form})

        return res.status(202).json({ message: 'Обновлено' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})



module.exports = router
