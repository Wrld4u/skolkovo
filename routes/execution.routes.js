const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Exec = db.execution

const router = Router()


// route prefix = /api/exec
//
// create exec
router.post('/create', auth, async (req, res) => {
        try {

            const { caseId, releaseId, userId, cycleId, status } = req.body

            const exec = await Exec.create({ caseId, releaseId, userId, cycleId, status })

            return res.status(201).json({ exec, message: 'Создано' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
    })

// // get project by id
// router.get('/:id', auth, async (req, res) => {
//     try {
//         const project = await Project.findByPk(req.params.id)
//         // console.log('=============', await project.getTags())
//         project.tags = await project.getTags()
//         project.cycles = await project.getCycles()
//         project.releases = await project.getReleases()
//         res.json({ project, message: 'ok' })
//     } catch (e) {
//         res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
//     }
// })
//
// update project by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { caseId, releaseId, userId, cycleId, status } = req.body

        const exec = await Exec.findByPk(req.params.id)
        await exec.update({ caseId, releaseId, userId, cycleId, status })

        await exec.save()

        return res.status(202).json({ exec, message: 'Обновлено' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// delete exec by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const exec = await Exec.findByPk(req.params.id)

        await exec.destroy()

        // todo delete all linked tables

        return res.status(202).json({ message: 'Удалено' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

module.exports = router