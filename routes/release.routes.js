const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Release = db.release
const Cycle = db.cycle
const Case = db.case
const Label = db.label
const Exec = db.execution

const router = Router()


const toISO = (d) => {
    d = d.split('.')
    return `${d[2]}-${d[1]}-${d[0]}`
}

// route prefix = /api/release
//
// create release
router.post('/create', auth, async (req, res) => {
        try {
            const { projectId, userId, name, status, cycles, start, end } = req.body


            console.log(start, end)
            const release = await Release.create({ projectId, userId, name, status, start: toISO(start), end: toISO(end) })

            if (cycles && cycles.length) {
                await release.setCycles([...cycles])
            }

            let cycle = await release.getCycles()
            if (cycle && cycle.length) {
                for (let i = 0; i < cycle.length; i++) {

                    let cases = await cycle[i].getCases()

                    if (cases && cases.length) {
                        for (let j = 0; j < cases.length; j++) {
                            await Exec.create({
                                caseId: cases[j].id,
                                releaseId: release.id,
                                userId: userId,
                                cycleId: cycle[i].id,
                                status: 'Unexecuted'
                            })
                        }

                    }

                }
            }

            return res.status(201).json({ message: 'Релиз создан' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
    })

// get all releases
router.post('/all', auth, async (req, res) => {
    try {
        const { projectId } = req.body

        let releases = null
        if (projectId) {
            releases = await Release.findAll({ where: {projectId}, include: [
                    {
                        model: Cycle,
                        include: [
                            {
                                model: Case,
                                include: [{
                                    model: Label,
                                    attributes: ['id', 'label']
                                }]
                            }, Exec
                        ]
                    }
                ] })
        } else {
            releases = await Release.findAll({ include: [
                    {
                        model: Cycle,
                        include: [
                            {
                                model: Case,
                                include: [{
                                    model: Label,
                                    attributes: ['id', 'label']
                                }]
                            }, Exec
                        ]
                    }
                ] })
        }

        res.json({ releases, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// get release by id
router.get('/:id', auth, async (req, res) => {
    try {

        let release = null

        if (!req.headers.details) {
            release = await Release.findOne({where: {id: req.params.id}, include: [
                    // {
                    //     model: Exec,
                    //     where: {releaseId: req.params.id}
                    // },
                ]})

            let cycles = await release.getCycles({attributes: ['id']})
            if (cycles.length) {
                cycles = cycles.map(el => el.id)
            } else {
                cycles = []
            }

            release.dataValues.cycles = cycles
        } else {
            release = await Release.findOne({where: {id: req.params.id}, include: [
                    {
                        model: Cycle,
                        include: [
                            {
                                model: Case,
                                include: [{
                                    model: Label,
                                    attributes: ['id', 'label']
                                }]
                            },
                            {
                                model: Exec,
                                where: {releaseId: req.params.id}
                            }
                        ]
                    }
                ]})
        }

        release.dataValues.start = release.dataValues.start.toLocaleDateString('ru-RU')
        release.dataValues.end = release.dataValues.end.toLocaleDateString('ru-RU')

        res.json({ release, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// update release by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { form } = req.body

        const release = await Release.findByPk(req.params.id)

        await release.update({
            userId: form.userId, start: toISO(form.start), end: toISO(form.end), name: form.name, status: form.status
        })

        let oldCycles = await release.getCycles({attributes: ['id']})
        if (oldCycles.length) {
            oldCycles = oldCycles.map(el => el.id)
        } else {
            oldCycles = []
        }

        if (form.cycles && form.cycles.length) {
            await release.setCycles([])
            await release.setCycles([...form.cycles])
        }

        let cycle = await release.getCycles()
        if (cycle && cycle.length) {
            for (let i = 0; i < cycle.length; i++) {

                if (!oldCycles.includes(cycle[i].id)) {
                    let cases = await cycle[i].getCases()

                    if (cases && cases.length) {
                        for (let j = 0; j < cases.length; j++) {
                            await Exec.create({
                                caseId: cases[j].id,
                                releaseId: release.id,
                                userId: form.userId,
                                cycleId: cycle[i].id,
                                status: 'Unexecuted'
                            })
                        }

                    }
                }

            }
        }



        await release.save()

        return res.status(202).json({ release, message: 'Обновлено' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// delete release by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const release = await Release.findByPk(req.params.id)

        await Exec.destroy({where: {releaseId: release.id}})

        await release.destroy()

        // todo delete all linked tables

        return res.status(202).json({ message: 'Удалено' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

router.post('/link', auth, async (req, res) => {
    try {
        const { cycleId, releaseId } = req.body

        const release = await Release.findByPk(releaseId)
        if (release) {
            // Link manyToMany Case - Cycle
            await release.addCycles([cycleId])
        }

        const c = await Cycle.findOne({where:{ id: cycleId }, include: [
                {
                    model: Case,
                    include: [{
                        model: Label,
                        attributes: ['id', 'label']
                    }]
                }, Exec
            ]})

        return res.status(201).json({ c, message: 'Linked' })

    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

router.post('/unlink', auth, async (req, res) => {
    try {
        const { cycleId, releaseId } = req.body

        const release = await Release.findByPk(releaseId)
        if (release) {
            // Link manyToMany Case - Cycle
            await release.removeCycles([cycleId])
        }

        return res.status(201).json({ message: 'Unlinked' })

    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})


module.exports = router