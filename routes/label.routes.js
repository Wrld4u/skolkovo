const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Label = db.label

const router = Router()


// route prefix = /api/label

// create label
router.post('/create', auth, async (req, res) => {
        try {
            const { label, color, projectId} = req.body
            const id = req.headers.id

            const lbl = await Label.create({ label, color, projectId})

            // set User
            // await project.setUsers([id])

            return res.status(201).json({ id: lbl.id, message: 'Метка создана' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
    })

// get labels
router.post('/all', auth, async (req, res) => {
    try {
        const { projectId } = req.body

        let lbl = null

        if (projectId) {
            lbl = await Label.findAll({ where: {projectId} })
        } else {
            lbl = await Label.findAll({})
        }

        res.json({ label:lbl, message: 'ok' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// update label by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { label, color } = req.body

        const lbl = await Label.findByPk(req.params.id)

        lbl.label = label
        lbl.color = color
        await lbl.save()

        return res.status(202).json({ label: lbl, message: 'Метка обновлена' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// delete label by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const lbl = await Label.findByPk(req.params.id)

        await lbl.destroy()

        // todo delete all linked tables

        return res.status(202).json({ message: 'Метка удалена' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

module.exports = router