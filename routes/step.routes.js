const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Step = db.step

const router = Router()


// route prefix = /api/step
//
// create exec
router.post('/create', auth, async (req, res) => {
        try {
            const { projectId, userId, name, status, description } = req.body

            const step = await Step.create({ projectId, userId, name, status, description })

            return res.status(201).json({ message: 'Step created' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
    })

// // get project by id
// router.get('/:id', auth, async (req, res) => {
//     try {
//         const project = await Project.findByPk(req.params.id)
//         // console.log('=============', await project.getTags())
//         project.tags = await project.getTags()
//         project.cycles = await project.getCycles()
//         project.releases = await project.getReleases()
//         res.json({ project, message: 'ok' })
//     } catch (e) {
//         res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
//     }
// })
//
// // update project by id
// router.put('/:id', auth, async (req, res) => {
//     try {
//         const { name } = req.body
//
//         const project = await Project.findByPk(req.params.id)
//
//         project.name = name
//         await project.save()
//
//         return res.status(202).json({ project, message: 'Проект обновлён' })
//     } catch (e) {
//         res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
//     }
// })

// delete exec by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const step = await Step.findByPk(req.params.id)
        console.log(step.id)
        await step.destroy()

        // todo delete all linked tables

        return res.status(202).json({ message: 'Step delete' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

module.exports = router