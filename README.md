**Requrement**
- node.js v14.15.1
- npm v 7.5.4
- MySQL 10.2.23-MariaDB
- pm2 manager (https://pm2.keymetrics.io/)

**Install**

1. Clone project to your directory
2. In project root run: \
 2.1 npm install \
 2.2 npm run client:install \
 2.3 npm run client:build 
 
**Run** 
- To run project (in project directory): pm2 start site.config.js

**Config**

_site.config.js_ 
1. env: Environment variables, here you can set Port of you application.
2. instances: Here you can set count of processor cores to use. \
more details - https://pm2.keymetrics.io/

_projectDir/config/production.json_ - in this file you can set DB settings, alternative port for application, base URL and jwt Token
